add_executable(example05-ex3-sol example05.cc)
target_link_libraries(example05-ex3-sol ${DUNE_LIBS})
set_target_properties(example05-ex3-sol
  PROPERTIES OUTPUT_NAME example05)
