template<int k0, int k1, class GV>
void example05_Qk0Qk1 (const GV& gv, double dtstart, double dtmax, double tend)
{
  // <<<1>>> Choose domain and range field type
  typedef typename GV::Grid::ctype Coord;
  typedef double Real;
  Real time = 0.0;

  // <<<2>>> Make two grid function spaces, one for each component of the system
  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,k0> FEM0;
  FEM0 fem0(gv);
  typedef Dune::PDELab::NoConstraints CON0;
  typedef Dune::PDELab::istl::VectorBackend<> VBE0;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM0,CON0,VBE0> GFS0;
  GFS0 gfs0(gv,fem0);
  gfs0.name("u0");

  typedef Dune::PDELab::QkLocalFiniteElementMap<GV,Coord,Real,k1> FEM1;
  FEM1 fem1(gv);
  typedef Dune::PDELab::NoConstraints CON1;
  typedef Dune::PDELab::istl::VectorBackend<> VBE1;
  typedef Dune::PDELab::GridFunctionSpace<GV,FEM1,CON1,VBE1> GFS1;
  GFS1 gfs1(gv,fem1);
  gfs1.name("u1");

  // composite GFS
  typedef Dune::PDELab::istl::VectorBackend<> VBE;
  typedef Dune::PDELab::LexicographicOrderingTag OrderingTag;
  typedef Dune::PDELab::CompositeGridFunctionSpace<VBE,OrderingTag,GFS0,GFS1> GFS;
  GFS gfs(gfs0,gfs1);
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  typedef Dune::PDELab::GridFunctionSubSpace
    <GFS,Dune::TypeTree::TreePath<0> > U0SUB;
  U0SUB u0sub(gfs);
  typedef Dune::PDELab::GridFunctionSubSpace
    <GFS,Dune::TypeTree::TreePath<1> > U1SUB;
  U1SUB u1sub(gfs);

  // <<<3>>> Make instationary grid operator
  Real d_0 = 0.00028, d_1 = 0.005;
  Real lambda = 1.0, sigma = 1.0, kappa = -0.05, tau = 0.1;
  typedef Example05LocalOperator LOP;
  LOP lop(d_0,d_1,lambda,sigma,kappa,2*std::max(k0,k1));
  typedef Example05TimeLocalOperator TLOP;
  TLOP tlop(tau,2*std::max(k0,k1));
  typedef Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
  MBE mbe(9+25);
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO0;
  GO0 go0(gfs,gfs,lop,mbe);
  typedef Dune::PDELab::GridOperator<GFS,GFS,TLOP,MBE,Real,Real,Real,CC,CC> GO1;
  GO1 go1(gfs,gfs,tlop,mbe);
  typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
  IGO igo(go0,go1);

  // How well did we estimate the number of entries per matrix row?
  // => print Jacobian pattern statistics (do not call this for IGO before osm.apply() was called!)
  typename GO0::Traits::Jacobian jac(go0);
  std::cout << jac.patternStatistics() << std::endl;

  // <<<4>>> Make FE function with initial value
  typedef typename IGO::Traits::Domain U;
  U uold(gfs,0.0);
  typedef U0Initial<GV,Real> U0InitialType;
  U0InitialType u0initial(gv);
  typedef U1Initial<GV,Real> U1InitialType;
  U1InitialType u1initial(gv);
  typedef Dune::PDELab::CompositeGridFunction<U0InitialType,U1InitialType> UInitialType;
  UInitialType uinitial(u0initial,u1initial);
  Dune::PDELab::interpolate(uinitial,gfs,uold);
  U unew(gfs,0.0);
  unew = uold;

  // <<<5>>> Select a linear solver backend
  typedef Dune::PDELab::ISTLBackend_SEQ_BCGS_SSOR LS;
  LS ls(5000,false);

  // <<<6>>> Solver for non-linear problem per stage
  typedef Dune::PDELab::Newton<IGO,LS,U> PDESOLVER;
  PDESOLVER pdesolver(igo,ls);
  pdesolver.setReassembleThreshold(0.0);
  pdesolver.setVerbosityLevel(2);
  pdesolver.setReduction(1e-10);
  pdesolver.setMinLinearReduction(1e-4);
  pdesolver.setMaxIterations(25);
  pdesolver.setLineSearchMaxIterations(10);

  // <<<7>>> time-stepper
  Dune::PDELab::Alexander2Parameter<Real> method;
  Dune::PDELab::OneStepMethod<Real,IGO,PDESOLVER,U,U> osm(method,igo,pdesolver);
  osm.setVerbosityLevel(2);

  // <<<8>>> graphics for initial guess
  std::stringstream basename;
  basename << "example05_Q" << k0 << "Q" << k1;

  typedef Dune::PDELab::DiscreteGridFunction<U0SUB,U> U0DGF;
  U0DGF u0dgf(u0sub,unew);
  typedef Dune::PDELab::DiscreteGridFunction<U1SUB,U> U1DGF;
  U1DGF u1dgf(u1sub,unew);

  Dune::VTKSequenceWriter<GV> vtkwriter(gv,basename.str(),"","bla");
  auto adapter_u0 = std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<U0DGF> >(u0dgf,"u0");
  auto adapter_u1 = std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<U1DGF> >(u1dgf,"u1");
  vtkwriter.addVertexData(adapter_u0);
  vtkwriter.addVertexData(adapter_u1);
  vtkwriter.write(time,Dune::VTK::appendedraw);

  // <<<9>>> time loop
  double dt = dtstart;
  while (time<tend-1e-8)
    {
      // do time step
      osm.apply(time,dt,uold,unew);

      // advance time step
      uold = unew;
      time += dt;
      if (dt<dtmax-1e-8)
        dt = std::min(dt*1.1,dtmax);

      // graphics
      vtkwriter.write(time,Dune::VTK::appendedraw);
    }
}
