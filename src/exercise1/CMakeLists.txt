add_executable(example05-ex1 example05.cc)
target_link_libraries(example05-ex1 ${DUNE_LIBS})
set_target_properties(example05-ex1
  PROPERTIES OUTPUT_NAME example05)
