\documentclass[american,a4paper]{article}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{ulem}
\usepackage{hyperref}

\lstset{language=C++, basicstyle=\ttfamily, 
  keywordstyle=\color{black}\bfseries, tabsize=4, stringstyle=\ttfamily,
  commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\title{\textbf{DUNE-PDELab exercises\\Friday, March~28th~2014\\16:00-17:30}}
\dozent{}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{DUNE Workshop 2013 Exercise Sheet}

\newcommand{\vx}{\vec x}
\newcommand{\grad}{\vec \nabla}
\renewcommand{\div}{\vec\nabla\cdot}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\vn}{\vec n}
\newcommand{\mycomment}[1]{}

\begin{document}

\blatt[8]{March~28th~2014}{}

Many temporal and spatial structures of systems in nature are not impressed from 
the outside, but they are rather induced by the particular system itself.

During exploration of such phaenomena of self-structuring 
systems it can be observed, 
that several of this systems can be described by 
the class of reaction-diffusion systems. The involved equations are 
two-component systems with  FitzHugh-Nagumo reaction:
\begin{subequations}
\begin{align*}
\partial_t u_0 - d_0 \Delta u_0 - f(u_0) + \sigma u_1 &= 0 &&\text{in $\Omega=(0,2)^2$},\\
\tau\partial_t u_1 - d_1 \Delta u_1 - u_0 + u_1 &= 0 &&\text{in $\Omega$},\\
\nabla u_0 \cdot n = \nabla u_1 \cdot n &= 0 &&\text{on $\partial\Omega$},\\
u_0(\cdot,t_0) &= U_0(\cdot), \\ 
u_1(\cdot,t_0) &= U_1(\cdot),
\end{align*}
\end{subequations}
with $f(u) = \lambda u - u^3 - \kappa$.

\begin{figure}[hbt]
\begin{center}
\hfill
\includegraphics[width=0.4\textwidth]{example05_Q1Q1-u0.pdf}
\hfill
\includegraphics[width=0.4\textwidth]{example05_Q1Q1-u1.pdf}
\hfill
\end{center}
\vspace*{-3cm}
\caption{
Solution for u0 (left) and u1 (right) for $Q_1$ 
Finite-Elements with parameter settings
{\tt d\_0 = 0.00028, d\_1 = 0.005,
  lambda = 1.0, sigma = 1.0, kappa = -0.05 and tau = 0.1} and
random initial conditions as preset in exercise1.}
\end{figure}

\vspace*{1cm}
\begin{uebung}{Understand the code structure.}

  \lstset{language=bash}
  The program is structured using the following files:
  \begin{itemize}
  \item \lstinline!example05.cc! -- main program,
  \item \lstinline!example05_QkQk.hh! -- driver to solve problem on a
    gridview with $Q_k$ Finite-Elements where $k$ is a template parameter, hard-codes $t_0=0$.
  \item \lstinline!example05_initial.hh! -- initial condition type
    function using random field generator,
  \item \lstinline!example05_operator.hh! -- spatial local
    operator $r(u,v;t)$
  \item \lstinline!example05_toperator.hh! -- temporal local
    operator $m(u,v;t)$
  \item \lstinline!permeability_generator.hh! -- generator for random initial conditions
  \end{itemize}

  Have a look at the individual source-code files, if you have not 
  done so far.
  Compile \lstinline!example05.cc! and run the program. Now, examine the 
  simulated result files with paraview. Here we use the class
  \lstinline{Dune::PVDWriter}
for writing time sequences. When you open the file
\lstinline{example05_Q1Q1.pvd} and use the
\lstinline{AnnotateTimeFilter} you can see the time of each frame
which is quite useful when using nonuniform time steps.
  
  If you run the program without arguments, it will print a short help
  message: 
  \begin{lstlisting}
usage: ./example05 <level> <dtstart> <dtmax> <tend>
  \end{lstlisting}
  \lstinline!<level>! is the number of
  \lstinline[language=C++]!globalRefine()!s the program should do after
  creating a $1\times1$ grid. \lstinline!<dtstart>! is the initial time step 
  size, \lstinline!<dtmax>! is the maximal allowed time step size, and
  \lstinline!<tend>! is the model time $T$, when to stop the simulation.

  Try various values for the command line parameters, until
  the selfstructuring process converges and its development 
  is finished!
\end{uebung}


\vspace*{1cm}
\begin{uebung}{$Q_2$ Finite Elements}
Change your program to use $Q_2$ ansatz functions! Compare the
runtime of the program using $Q_1$ and $Q_2$ 
Finite Elements for a fixed grid level. (Unfortunately, the
\lstinline{Dune::PVDWriter} does not support subsampling, so you do
not ``see'' the higher order elements'').
\end{uebung}


\vspace*{1cm}
\begin{uebung}{Missed woodstock? Have your own experience!}
Change the parameters in the file 
{\tt example\_QkQk.hh} to\\
{\tt d\_0 = 0.000964, d\_1 = 0.0001, lambda = 0.9, 
sigma = 1.0, kappa = 0 and tau = 4.0}.

Now, run the simulation again with similar values for
{\tt <level>}, $\ldots$  as you have choosen in exercise1.

Generate a movie from the image sequence.
Take your time and watch the movie, see the periodically 
selfstructuring system and enjoy the music of {\it Jimi Hendrix}
(\url{http://www.youtube.com/watch?v=jmVcRxFUhEQ})\\
More about woodstock \url{http://www.youtube.com/watch?v=Vv98-4eOJbU}.
\end{uebung}


\vspace*{1cm}
\begin{uebung}{Alternate type of self structuredness!}
To get a periodically reoccuring less psychedelic solution in form of
a travelling wave, you need to choose the 
parameters as in the previous exercise {\tt (d\_0 = 0.000964,
d\_1 = 0.0001, lambda = 0.9, sigma = 1.0, kappa = 0 and tau = 4.0)},
but alternate initial conditions.\\ 
Try instead of the random initial field the 
following symmetric functions for u0 and u1:\\
%{\tt
%    Dune::FieldVector<ctype,dim> midpoint(1.0);\\
%    x -= midpoint;\\
%    if (x.infinity\_norm()<=0.5)\\
%      y = 1.0;\\
%    else\\
%      y = 0.0;\\
%}
%and u1\\ 
%{\tt
%    Dune::FieldVector<ctype,dim> midpoint(1.0);\\
%    x -= midpoint;\\
%    if (x.infinity\_norm()<=0.4)\\
%      y = -11.0;\\
%    else\\
%      y = 0.0;\\
%}
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.8\textwidth]{initialconditions.pdf}
\end{center}
%\vspace*{-3cm}
%\caption{
%Initial solution for u0 (left) and u1 (right) for 
%travelling wave solution.}
\end{figure}

Extend and compile your program. To simulate choose a larger value 
for {\tt tend}, approximately double
the time as in the previous exercises and run the program.  
Have a look at the elegant periodic traveling wave solution 
with paraview. Experiment with other, e.g. sinusoidal, initial conditions
and see what happens!
\end{uebung}


\vspace*{1cm}
\begin{uebung}{$Q_1-Q_2$ Finite Elements}

\begin{figure}[h]
\begin{center}
\hfill
\includegraphics[width=0.4\textwidth]{example05_Q1Q2-u0_tw.pdf}
\hfill
\includegraphics[width=0.4\textwidth]{example05_Q1Q2-u1_tww.pdf}
\hfill
\end{center}
\vspace*{-3cm}
\caption{
Solution for u0 (left) and u1 (right) for $Q_1-Q_2$ 
Finite-Elements at $t=1$ with parameter settings
{\tt d\_0 = 0.00028, d\_1 = 0.005,
  lambda = 1.0, sigma = 1.0, kappa = -0.05 and tau = 0.1} and
initial conditions as in the previous exercise.}
\end{figure}

Extend the $Q_1-Q_1$ FE-Space to two different finite elements spaces
for the two different components, i.e.
choose $Q_2$ ansatz functions for the second component
instead of $Q_1$. Simulate the system and visualize
the solution!

Most probably you will not find out this on your own. The trick is to
use \lstinline{Dune::PDELab::CompositeGridFunctionSpace} instead of
\lstinline{Dune::PDELab::PowerGridFunctionSpace}. Have a look at the
solution provided.
\end{uebung}


If you need a further challenge then change the code to three
dimensions and use parallel
computing. Example 6 in \lstinline{dune-pdelab-howto/src/course-examples/example06}
provides a parallelization of the two-dimensional reaction-diffusion
system as a starting point. 

% \vspace*{1cm}
% \begin{uebung}{Take the long \xout{way} work home: Self structuring systems in 3D!}
% Now extend the example to 3D. Setup a new problem
% showing its own phenomena of self-structuredness: 
% choose a domain, appropriate parameters and initial 
% conditions.\\
% Compute your 3D simulation in parallel for different
% processor counts and analyze the speedup behaviour
% when solving your timedependent, nonlinear and coupled two-component 
% equation system.\\
% No reference solution, it is up to you!
% \end{uebung}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% mode: TeX-PDF
%%% mode: auto-fill
%%% TeX-master: t
%%% mode: flyspell
%%% ispell-local-dictionary: "american"
%%% End: 
